const counterFactory = require("../counterFactory.cjs");

try {
    let testObject = counterFactory();
    console.log(testObject.increment());
    console.log(testObject.increment());
    console.log(testObject.increment());
    console.log(testObject.decrement());
    console.log(testObject.decrement());

} catch (error) {
    console.log(error);
}