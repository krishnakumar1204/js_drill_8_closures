const cacheFunction = require("../cacheFunction.cjs");

try {
    let testFunction = cacheFunction((number) => {

        // console.log("Inside cb function"); // To check if it's invoking the 'cb' function or not
        return 2*number;
    });
    
    console.log(testFunction(2)); // this will invoke the 'cb' function as there is no cached value corresponding to 2

    console.log(testFunction(6)); // this will invoke the 'cb' function as there is no cached value corresponding to 6

    console.log(testFunction(2)); // this will not invoke the 'cb' function as there is a cached value corresponding to 2

} catch (error) {
    console.log(error);
}