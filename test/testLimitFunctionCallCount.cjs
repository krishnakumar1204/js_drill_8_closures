const limitFunctionCallCount = require("../limitFunctionCallCount.cjs");

try {
    let limitingFunction = limitFunctionCallCount(() => console.log("Hello"), 2);

    // these two will invoke the 'cb' function
    limitingFunction();
    limitingFunction();


    // this will not invoke the 'cb' function as it has reached the limit
    limitingFunction();

    
    // it will return 'null'
    console.log(limitingFunction());


} catch (error) {
    console.log(error);
}