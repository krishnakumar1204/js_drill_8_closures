function limitFunctionCallCount(cb, n) {
    // Should return a function that invokes `cb`.
    // The returned function should only allow `cb` to be invoked `n` times.
    // Returning null is acceptable if cb can't be returned

    let count=0;

    let limitingFunction = function(){

        if(count<n){
            cb();
            count++;
        }
        else{
            console.log("Limit Exceeded!");
            return null;
        }
    };

    return limitingFunction;
}

module.exports = limitFunctionCallCount;